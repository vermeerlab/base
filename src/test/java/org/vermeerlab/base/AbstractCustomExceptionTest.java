/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.base;

import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author Yamashita,Takahiro
 */
public class AbstractCustomExceptionTest {

    @Test
    public void testAbstractCustomException() {
        AbstractCustomException ex = new AbstractCustomException("test") {
            private static final long serialVersionUID = 1L;
        };

        String message = ex.getMessage();
        Assert.assertEquals(message, "test");
    }

    @Test
    public void testAbstractCustomExceptionWithException() {

        Exception exception = new Exception("aaa");

        AbstractCustomException ex = new AbstractCustomException(exception, "test") {
            private static final long serialVersionUID = 1L;
        };

        String message = ex.getMessage();
        Assert.assertEquals(message, "test"+System.lineSeparator());

        String detail = ex.getCause().getMessage();
        Assert.assertEquals(detail, "aaa");
    }

}
