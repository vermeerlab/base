package org.vermeerlab.base;

/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.List;
import org.junit.After;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Yamashita,Takahiro
 */
public class DebugUtilTest {

    private ByteArrayOutputStream _baos;
    private PrintStream _out;

    @Before
    public void setUp() throws FileNotFoundException {
        _baos = new ByteArrayOutputStream();
        _out = System.out;
        System.setOut(
                new PrintStream(
                        new BufferedOutputStream(_baos)
                )
        );
    }

    @After public void tearDown() {
        System.setOut(_out);
    }

    private String resultValue() {
        String result = _baos.toString();
        return result == null
               ? ""
               : result.replaceAll("\r|\n", "");
    }

    @Test public void インスタンス化() {
        DebugUtil util = new DebugUtil(true);
    }

    @Test
    public void 単体デバッグ_ON_出力検証() {
        Boolean isDebug = true;
        String message = "test";
        // Act
        DebugUtil util = new DebugUtil(isDebug);
        util.print(message);
        System.out.flush();
        assertEquals(message, resultValue());
    }

    @Test
    public void 単体デバッグ_OFF_出力検証() {
        Boolean isDebug = false;
        String message = "test";
        // Act
        DebugUtil util = new DebugUtil(isDebug);
        util.print(message);
        System.out.flush();
        assertEquals("", resultValue());
    }

    @Test
    public void 複数デバッグ_ON_出力されない検証() {
        Boolean isDebug = true;
        List<String> messages = Arrays.asList("test1", "test2");
        // Act
        DebugUtil util = new DebugUtil(isDebug);
        util.print(messages);
        System.out.flush();
        assertEquals("test1test2", resultValue());
    }

    @Test
    public void 複数デバッグ_OFF_出力されない検証() {
        Boolean isDebug = false;
        List<String> messages = Arrays.asList("test1", "test2");
        // Act
        DebugUtil util = new DebugUtil(isDebug);
        util.print(messages);
        System.out.flush();
        assertEquals("", resultValue());
    }
}
