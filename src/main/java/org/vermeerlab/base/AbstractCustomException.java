/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.base;

/**
 * 独自クラス群に指定する実行時例外の基底クラス
 *
 * @author Yamashita,Takahiro
 */
public abstract class AbstractCustomException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    /**
     * 実行時例外を構築します.
     *
     * @param messages 例外情報に付与する文字列
     */
    public AbstractCustomException(String... messages) {
        super(String.join(System.lineSeparator(), messages));
    }

    /**
     * メッセージに加えて実行時例外情報のスタックトレース情報を付与した実行時例外を構築します.
     * <P>
     * @param ex 例外
     * @param messages 例外情報に付与する文字列
     */
    public AbstractCustomException(Exception ex, String... messages) {
        super(new StringBuilder()
                .append(String.join(System.lineSeparator(), messages))
                .append(System.lineSeparator())
                .toString(), ex
        );
    }
}
