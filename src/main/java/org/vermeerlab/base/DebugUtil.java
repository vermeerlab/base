/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.base;

import java.util.List;

/**
 * デバッグ用のヘルパー.
 * <P>
 * 簡易確認として標準出力をする機能です.<br>
 * プロダクトでは使用することは想定せず、テスト時の疎通確認などで使用することを想定しています.<br>
 *
 * @author Yamashita,Takahiro
 */
public class DebugUtil {

    Boolean isDebug;

    /**
     * For expansion
     *
     * @param isDebug true：デバッグ指定あり
     */
    public DebugUtil(Boolean isDebug) {
        this.isDebug = isDebug;
    }

    /**
     * デバッグログを標準出力に出力します.
     *
     * @param message 出力メッセージ
     */
    public void print(String message) {
        if (this.isDebug == false) {
            return;
        }
        System.out.println(message);
    }

    /**
     * デバッグログを標準出力に出力します.
     *
     * @param messages 出力メッセージの対象オブジェクトリスト
     */
    public void print(List<?> messages) {
        if (isDebug == false) {
            return;
        }
        messages.stream().map(Object::toString).forEach(System.out::println);
    }
}
